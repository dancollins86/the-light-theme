
// Hide Page Loader when DOM and images are ready
jQuery(document).ready(function ($) {
  $(window).on('load', () => $('.pageloader').removeClass('is-active'));
});


function isElementInViewport(e) {
  var t = e.getBoundingClientRect(),
    n = window.innerHeight || document.documentElement.clientHeight,
    i = window.innerWidth || document.documentElement.clientWidth;
  return (
    t.top >= 0 &&
    t.left >= 0 &&
    t.bottom <= (n) &&
    t.right <= (i)
  );
}

window.onscroll = function () {
  var e = document.querySelector(".top-nav");
  isElementInViewport(document.querySelector(".overlay-nav")) ? e.classList.remove("show") : e.classList.add("show");
};


var toggles = document.querySelectorAll(".nav-toggle");
toggles.forEach(function (e) {
  e.onclick = function () {
    this.classList.toggle("active");
    var e = document.querySelector("body"),
      t = document.querySelector(".nav-menu");
    document.querySelector(".nav-menu").classList.contains("open") ? (e.classList.remove("has-overlay"), t.style.display = "none", t.classList.remove("open")) : (e.classList.add("has-overlay"), t.style.display = "block", setTimeout(function () {
      t.classList.add("open")
    }, 100))
  }
});
