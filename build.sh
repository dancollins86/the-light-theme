#!/bin/bash

echo "Starting production build..."

set -e

echo "    Bundling JS & CSS"
npm run prod

wait $(jobs -p)
echo "Finished Build"



