#!/bin/bash

echo "Start Lint"

set -e

echo "    Linting SASS code"
./node_modules/sass-lint/bin/sass-lint.js -c .sasslintrc -v -q

wait $(jobs -p)
echo "Finished Lint"



