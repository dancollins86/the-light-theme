<?php
wp_enqueue_style('material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons');

add_filter('timber_context', 'thelight_timber_context');

function thelight_timber_context( $context ) {
	$context['options'] = get_fields('option');
	return $context;
}

 // Bootstrapping theme
require_once __DIR__ . '/app/bootstrap.php';

wp_enqueue_script('slick', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', array('jquery'));


if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}

// Shortcode to output custom PHP in Visual Composer
// function wpc_vc_shortcode($atts)
// {
//     if (isset($_GET['price'])) {
//         $package_price = $_GET['price'];
//     }

//     if (isset($_GET['package'])) {
//         $package = $_GET['package'];
//     }

//     if($package) {
//         echo '<div class="submit-vacancy-package-options">' .
//             '<div class="submit-vacancy-package-options__title">Your Online Recruitment Package</div>' .
//             '<ul class="submit-vacancy-package-options__list">' .
//             '<li>' . $package .'</li>' .
//         '</ul>';
//     }
//     if ($package_price) {
//         echo '<div class="submit-vacancy-package-options__footer">' .
//             'Package Total = <span>£' .$package_price . '</span>' .
//             '</div>';
//     }

//     if($package) {
//         echo '</div>';
//     }

// }
// add_shortcode('my_vc_php_output', 'wpc_vc_shortcode');

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );


// Register Custom Post Type
function reg_work_custom_post_type()
{
    $labels = array(
        'name'                  => _x('work', 'Work', 'work'),
        'singular_name'         => _x('work', 'Work', 'work'),
        'menu_name'             => __('Work', 'work'),
        'name_admin_bar'        => __('Add New', 'work'),
        'archives'              => __('Item Archives', 'work'),
        'parent_item_colon'     => __('Parent Item', 'work'),
        'all_items'             => __('All Items', 'work'),
        'add_new_item'          => __('Add New Item', 'work'),
        'add_new'               => __('Add New', 'work'),
        'new_item'              => __('Not Found', 'work'),
        'edit_item'             => __('Edit Item', 'work'),
        'update_item'           => __('Update Item', 'work'),
        'view_item'             => __('View Item', 'work'),
        'search_items'          => __('Search Item', 'work'),
        'not_found'             => __('Not Found', 'work'),
        'not_found_in_trash'    => __('Not Found In Trash', 'work'),
        'featured_image'        => __('Featured Image', 'work'),
        'set_featured_image'    => __('Set Featured Image', 'work'),
        'remove_featured_image' => __('Remove Featured Image', 'work'),
        'use_featured_image'    => __('Use As Featured Image', 'work'),
        'insert_into_item'      => __('Work', 'work'),
        'uploaded_to_this_item' => __('Uploaded To This Item', 'work'),
        'items_list'            => __('Items List', 'work'),
        'items_list_navigation' => __('Items List Navigation', 'work'),
        'filter_items_list'     => __('Filter Items List', 'work'),
    );
    $args = array(
        'label'                 => __('Post Type', 'work'),
        'description'           => __('Post Type Description', 'work'),
        'labels'                => $labels,
        'supports'              => array('title', 'editor', 'trackbacks', 'content', 'revisions', 'excerpt', 'custom_fields', 'author', 'post_attributes', 'featured_image', 'post_formats', 'comments'),
        'taxonomies'            => array(''),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type('work', $args);
}
add_action('init', 'reg_work_custom_post_type', 0);
